# Example - C++ Executable

An example for a simple C++ console application.

Exercise:

Take a whole number passed in by the user, add 3 to the number, and print it on the console.

Example solution:

```c++
#include <iostream>
#include <string>
#include <cstdio>

// Take a whole number passed in by the user, add 3 to the number, and print it on the console.

int main()
{
    int a;
    std::cin >> a;
    std::cout << a + 3;
}
```

Example tester:

```py
from py_eval_util import Evaluator

def test_file():
    # List of test cases
    test_cases = [
        (0, 3),
        (3, 6),
        (-3, 0),
        (-6, -3),
    ]

    # Function that takes question id and result and returns a score between 0 and 1
    def score_provider(i, result):
        try:
            res = int(result[0])
            return res == test_cases[i][1]
        except:
            return 0        

    e = Evaluator()
    # Set the name for the question. Evaluator will start calling at i = 0 and increase it.
    # If None is returned, that marks the limit to the number of questions
    e.with_name(
        lambda i: f"Numeric user input: {test_cases[i][0]}" if i < len(test_cases) else None)
    # Specify that an executable will be ran
    e.run_executable()
    # Set the console input (stdin) for a given test case
    e.with_input(lambda i: test_cases[i][0])
    # Let the score provider decide a score for the result
    e.with_score(score_provider)
    # Give feedback based on result and score
    e.with_feedback(
        lambda i, result, score: "Output: {}, Expected: {} : {}".format(', '.join(result), test_cases[i][1], "PASS" if score >= 1 else "FAIL"))
    # Run the evaluator
    e.start()

if __name__ == "__main__":
    test_file()
```

Example config file:

```json
{
    "tests": [
        {
            "type": "compile",
            "max_score": 10.0,
            "number": "1",
            "tags": [],
            "visibility": "visible"
        },
        {
            "type": "functionality_executable",
            "max_score": 60.0,
            "number": "2",
            "tags": [],
            "visibility": "visible",
            "tester_file": "path/to/tester.py"
        },
        {
            "type": "static",
            "max_score": 10.0,
            "number": "3",
            "tags": [],
            "visibility": "visible"
        },
        {
            "type": "comments",
            "max_score": 10.0,
            "number": "4",
            "tags": [],
            "visibility": "visible"
        },
        {
            "type": "style",
            "max_score": 10.0,
            "number": "5",
            "tags": [],
            "visibility": "visible",
            "style": "google"
        }
    ]
}
```