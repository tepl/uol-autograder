# Example - Python Function

An example for testing a Python function

Exercise:

Implement the factorial function that for a given non-zero whole number N return the factorial of N (1 * 2 * 3 * ... N)

Example empty `.py` file:

```py
# Implement the factorial function that for a given non-zero whole number N return the factorial of N (1 * 2 * 3 * ... N)

def fibonacci(n):
    
```

Example solved `.py` file:
```py
def fibonacci(n):
    # First Fibonacci number is 0 
    if n <= 1: 
        return 0
    # Second Fibonacci number is 1 
    elif n==2: 
        return 1
    else: 
        return fibonacci(n-1) + fibonacci(n-2)
```

Example tester:

```py
from py_eval_util import Evaluator
import random

def test_file():
    # Reference function, used for signiture and behaviour
    def fibonacci(n):
        if n<=1: 
            return 0
        elif n==2: 
            return 1
        else: 
            return fibonacci(n-1) + fibonacci(n-2)
    # Create test cases
    test_cases = random.sample(list(range(-5, 20)), 10)

    e = Evaluator()
    # Set the name for the question. Evaluator will start calling at i = 0 and increase it.
    # If None is returned, that marks the limit to the number of questions
    e.with_name(
        lambda i: f"fibonacci({test_cases[i]})" if i < len(test_cases) else None)
    # Run code enables running a specific part of the module
    # The executed part is identified by the signature, which must in name and arguments
    # The executed part is passed in as the second argument
    e.run_code(
        lambda i, f: f(test_cases[i]), signature=fibonacci)
    # Inplace score provider that checks the output to the reference function
    e.with_score(
        lambda i, result: result == fibonacci(test_cases[i]))
    # Give feedback based on result and score
    e.with_feedback(
        lambda i, result, score: "Output: {}, Expected: {} : {}".format(result, fibonacci(test_cases[i]), "PASS" if score >= 1 else "FAIL"))
    # Run the evaluator
    e.start()


if __name__ == "__main__":
    test_file()
```

Example config file:

```json
{
    "tests": [
        {
            "type": "syntax",
            "max_score": 10.0,
            "number": "1",
            "tags": [],
            "visibility": "visible"
        },
        {
            "type": "functionality",
            "max_score": 70.0,
            "number": "2",
            "tags": [],
            "visibility": "visible",
            "tester_file": "Py/BasicMath/fibonacci_tester.py"
        },
        {
            "type": "comments",
            "max_score": 10.0,
            "number": "3",
            "tags": [],
            "visibility": "visible"
        },
        {
            "type": "static_analysis",
            "max_score": 10.0,
            "number": "4",
            "tags": [],
            "visibility": "visible"
        }
    ]
}
```