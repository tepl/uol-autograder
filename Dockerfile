FROM python:3.8.6

RUN apt-get update
RUN apt-get install -y cloc cppcheck clang-format

WORKDIR /setup

COPY ./requirements.txt /setup
RUN pip install -r requirements.txt

WORKDIR /app
