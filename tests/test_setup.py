from pathlib import Path
from collections import namedtuple
from tempfile import TemporaryDirectory
from pytest import raises
from tests import TEST_DIR
from tests.fixtures import setup_files
from feedback import load_config, TmpFiles, get_test_runner
from feedback.cpp import CppRunner
from feedback.py import PyRunner
from feedback.general.util import get_files_in_dir


def test_load_config(setup_files):
    config_file = setup_files.test_config

    config = load_config(config_file)

    assert hasattr(config, "tests")

    assert hasattr(config.tests, "compile")
    assert hasattr(config.tests.compile, "weight")
    assert config.tests.compile.weight == 0.1
    assert hasattr(config.tests.compile, "type")
    assert config.tests.compile.type == "o"

    assert hasattr(config.tests, "functionality")
    assert hasattr(config.tests, "comments")
    assert hasattr(config.tests, "static")
    assert hasattr(config.tests, "style")
    
    assert hasattr(config, "execution")
    assert hasattr(config.execution, "tester_file")
    assert config.execution.tester_file == "run_code.cpp"

def test_tmpfiles(setup_files):
    Config = namedtuple("Config", "execution")
    Execution = namedtuple("Execution", "tester_file")
    config = Config(Execution(
        setup_files.tmp_files.tester_file.absolute().as_posix()
    ))

    tested_file = setup_files.tmp_files.tested_file

    tmp_files = TmpFiles(tested_file, config)

    assert tmp_files.lookup_dir.is_dir()
    assert len(get_files_in_dir(tmp_files.lookup_dir)) == 8

    assert isinstance(tmp_files.tmp_dir, TemporaryDirectory)

    tmp_dir = Path(tmp_files.tmp_dir.name)
    copied_files = get_files_in_dir(tmp_dir)
    copied_file_names = [file_path.name for file_path in copied_files]
    assert len(copied_files) == 5
    assert all(f in copied_file_names 
               for f in [setup_files.tmp_files.tester_file.name,
                         setup_files.tmp_files.additional_file_0.name,
                         setup_files.tmp_files.additional_file_1.name,
                         setup_files.tmp_files.tested_file.name] )

    tmp_files.teardown()
    assert not tmp_dir.is_dir()

def test_get_test_runner_cpp():
    assert get_test_runner(Path("file.cpp")) == CppRunner

def test_get_test_runner_py():
    assert get_test_runner(Path("file.py")) == PyRunner

def test_get_test_runner_unknown():
    with raises(Exception):
        get_test_runner(Path("file.doc"))