import os, json
import pytest
from pathlib import Path
from tests import TEST_DIR
from tests.fixtures import integration_files, _integration_files
from feedback import run_test
import subprocess
from tempfile import TemporaryDirectory


def compare_results(d1, d2, ignore_keys=[], do_print=False):
    if do_print:
        print(json.dumps(d1, indent=4))
        print(json.dumps(d2, indent=4))

    if d1.keys() != d2.keys():
        return d1.Keys() == d2.Keys()
    
    assert "tests" in d1 and "tests" in d2

    d1_tests = [dict((k, v) for k,v in test.items() if k not in ignore_keys) for test in d1["tests"]]
    d2_tests = [dict((k, v) for k,v in test.items() if k not in ignore_keys) for test in d2["tests"]]
    
    return d1_tests == d2_tests

def load_result(path):
    try:
        with path.open() as f:
            return json.load(f)
    except json.decoder.JSONDecodeError:
        return {}

def save_result(path, result):
    with path.open('w') as f:
        json.dump(result, f)

def update_config_paths(path, tmpdir):
    with path.open() as f:
        config = json.load(f)
    

    assert "tests" in config
    for test in config["tests"]:
        if "tester_file" in test:
            test["tester_file"] = Path(TEST_DIR, "..", test["tester_file"]).absolute().as_posix()
    
    new_path = Path(tmpdir, "config.json")
    with new_path.open("w") as f:
        json.dump(config, f)
    return new_path


@pytest.mark.slow
@pytest.mark.parametrize(
    "tested_file,config_file,expected_file,run_args",
    [
        pytest.param(_integration_files.py.tested_file, _integration_files.py.config_file, _integration_files.py.expected_file, {}, id="py"),
        pytest.param(_integration_files.py_exploit.tested_file, _integration_files.py_exploit.config_file, _integration_files.py_exploit.expected_file, {}, id="py_exploit"),
        pytest.param(_integration_files.py_env.tested_file, _integration_files.py_env.config_file, _integration_files.py_env.expected_file, {}, id="py_env"),
        pytest.param(_integration_files.cpp_o.tested_file, _integration_files.cpp_o.config_file, _integration_files.cpp_o.expected_file, {"clear_cache": True, "check_validity": False}, id="cpp_o"),
        pytest.param(_integration_files.cpp_o.tested_file, _integration_files.cpp_o.config_file, _integration_files.cpp_o.expected_file, {"check_validity": False}, id="cpp_o_cached"),
        pytest.param(_integration_files.cpp.tested_file, _integration_files.cpp.config_file, _integration_files.cpp.expected_file, {"check_validity": False}, id="cpp"),
        pytest.param(_integration_files.cpp_executable.tested_file, _integration_files.cpp_executable.config_file, _integration_files.cpp_executable.expected_file, {}, id="cpp_executable"),
        pytest.param(_integration_files.cpp_timeout.tested_file, _integration_files.cpp_timeout.config_file, _integration_files.cpp_timeout.expected_file, {}, id="cpp_timeout"),
        pytest.param(_integration_files.cpp_memory.tested_file, _integration_files.cpp_memory.config_file, _integration_files.cpp_memory.expected_file, {}, id="cpp_memory"),
        pytest.param(_integration_files.cpp_17std.tested_file, _integration_files.cpp_17std.config_file, _integration_files.cpp_17std.expected_file, {"check_validity": False}, id="cpp"),
    ]
)
def test_integration(tested_file, config_file, expected_file, run_args, tmpdir):
    config_file = update_config_paths(config_file, tmpdir)

    result = run_test(tested_file, config_file, run_args, precision=2)

    assert result

    if not compare_results(result, load_result(expected_file), ignore_keys=["output"]):
        with TemporaryDirectory() as d:
            res_file = Path(d, "result.json") 
            with res_file.open("w") as f:
                json.dump(result, f, indent=4)
            try:
                subprocess.call(["code", "-d", res_file.absolute().as_posix(), expected_file.absolute().as_posix(), "-n", "-w"], shell=True)
            except:
                pass
        assert compare_results(result, load_result(expected_file), do_print=True, ignore_keys=["output"])
