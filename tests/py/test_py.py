import pytest
from pathlib import Path
from tests.fixtures import _py_files
from feedback import load_config, TmpFiles
from feedback.py import PyRunner, PyLookup
from feedback.general.util import dict_to_namedtuple

@pytest.mark.parametrize(
    "tested_file,max_score,expected_mark",
    [
        pytest.param(_py_files.syntax.correct, 10, 1, id="correct"),
        pytest.param(_py_files.syntax.incorrect, 50, 0, id="incorrect")
    ]
)
def test_syntax_check(tested_file, max_score, expected_mark):
    config = dict_to_namedtuple({
        "tests":[
            {
                "max_score": max_score,
                "type": "syntax"
            }
        ]})

    tmp_files = TmpFiles(tested_file, config)
    runner = PyRunner(tmp_files)

    runner.test_syntax(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks

    assert len(feedbacks) == 1
    feedback = feedbacks[0]

    assert feedback.name == "syntax"
    assert feedback.max_score == max_score
    assert feedback.score == round(max_score * expected_mark, 2)

    tmp_files.teardown()


@pytest.mark.parametrize(
    "tested_file,tester_file,max_score,expected_marks",
    [
        pytest.param(_py_files.functionality.tested_0, _py_files.functionality.tester, 10, [
            {"name": "test_0", "max_score": 1, "score": 1}
        ], id="single_result"),
        pytest.param(_py_files.functionality.tested_1, _py_files.functionality.tester, 20, [
            {"name": "sumdiff", "max_score": 0.4, "score": 0.5},
            {"name": "fibonacci", "max_score": 0.6, "score": 0.8}
        ], id="multiple_values"),
        pytest.param(_py_files.functionality.tested_2, _py_files.functionality.tester, 10, [
            {"name": "test1", "max_score": 0.2, "score": 0.5},
            {"name": "test2", "max_score": 0.8, "score": 0.8},
            {"name": "test3", "max_score": 0.0, "score": 0},
        ], id="multiple_values_with_zero")
    ]
)
def test_functionality(tested_file, tester_file, max_score, expected_marks):
    config = dict_to_namedtuple({
        "tests":[
                {
                    "type": "functionality",
                    "max_score": max_score,
                    "tester_file": tester_file
                }
        ]})

    tmp_files = TmpFiles(tested_file, config)
    runner = PyRunner(tmp_files)
    runner.check_validity = False

    runner.test_functionality(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks

    assert len(feedbacks) == len(expected_marks)
    for expected, real in zip(expected_marks, feedbacks):
        assert real.name == expected["name"]
        assert real.max_score == round(expected["max_score"] * max_score, 2)
        assert real.score == round(expected["score"] * expected["max_score"] * max_score, 2)

    

    tmp_files.teardown()

def test_lookup():
    tmp_files = TmpFiles(_py_files.functionality.tested_0, None)
    lookup = PyLookup(tmp_files.lookup_dir)

    assert lookup.runner_feedback.is_file()
    assert lookup.eval_feedback.is_file()


@pytest.mark.parametrize(
    "tested_file, max_score, expected_mark, error_penalty, warning_penalty, convention_penalty, refactor_penalty",
    [
        pytest.param(_py_files.static.correct, 100, 1, None, None, None, None, id="correct"),
        pytest.param(_py_files.static.warnings, 10, 0.55, None, None, None, None, id="warnings"),
        pytest.param(_py_files.static.errors, 15, 0, None, None, None, None, id="errors"),
        pytest.param(_py_files.static.warnings, 10, 0.91, None, 0.02, 0.01, 0.0, id="warnings_changed_penalty"),
        pytest.param(_py_files.static.errors, 20, 0.5, 0.5, None, None, None, id="errors_with_changed_penalty"),
        pytest.param(_py_files.static.warnings, 5, 0, None, 0.5, 0.5, 0.5, id="warnings_penalty_cap"),
        pytest.param(_py_files.static.warnings, 50, 1, None, -0.5, -0.5, -0.5, id="warnings_reverse_penalty_cap"),
    ]
)
def test_static_analysis(tested_file, max_score, expected_mark, error_penalty, warning_penalty, convention_penalty, refactor_penalty):
    config = dict_to_namedtuple({
        "tests":[
            {
                **{"type":"static", "max_score": max_score},
                **({"error_penalty": error_penalty} if error_penalty is not None else {}),
                **({"warning_penalty": warning_penalty} if warning_penalty is not None else {}),
                **({"convention_penalty": convention_penalty} if convention_penalty is not None else {}),
                **({"refactor_penalty": refactor_penalty} if refactor_penalty is not None else {})
            }
    ]})

    tmp_files = TmpFiles(tested_file, config)
    runner = PyRunner(tmp_files)

    runner.static_analysis(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks
    assert len(feedbacks) == 1
    feedback = feedbacks[0]

    assert feedback.name == "static analysis"
    assert feedback.max_score == max_score
    assert feedback.score == round(expected_mark * max_score, 2)

    tmp_files.teardown()