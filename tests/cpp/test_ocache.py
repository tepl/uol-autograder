import pytest
from shutil import copy
from pathlib import Path
from tests.fixtures import _cpp_files
from feedback.cpp.ocache import OCache
from feedback.general.util import get_files_in_dir

def test_cache_compile():
    ocache = OCache()
    ocache.clear()

    tester = _cpp_files.tester1

    assert ocache.get(tester) is None
    ocache.add(tester)

    assert ocache.get(tester) is not None
    assert ocache.get(tester).is_file()
    assert ocache.get(tester).suffix == ".o"


def test_cache_clear():
    ocache = OCache()
    ocache.clear()

    tester = _cpp_files.tester1

    assert ocache.get(tester) is None
    ocache.add(tester)

    assert ocache.get(tester) is not None
    assert len(get_files_in_dir(ocache._cache_dir)) == 1
    
    ocache.clear()

    assert ocache.get(tester) is None
    assert len(get_files_in_dir(ocache._cache_dir)) == 0

def test_cache_discovery():
    ocache = OCache()
    ocache.clear()

    cache_dir = ocache._cache_dir
    assert cache_dir.is_dir()
    
    tester = _cpp_files.tester1
    target = Path(cache_dir, ocache._get_hashed_name(tester))
    
    del ocache
    copy(tester, target)

    ocache = OCache()
    assert ocache.get(tester) is not None
