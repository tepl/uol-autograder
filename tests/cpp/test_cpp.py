import pytest
from pathlib import Path
from tests.fixtures import _cpp_files
from feedback import load_config, TmpFiles
from feedback.cpp import CppRunner
from feedback.general.util import dict_to_namedtuple

@pytest.mark.parametrize(
    "tested_file,max_score,warning_penalty,expected_mark,check_type",
    [
        pytest.param(_cpp_files.main1, 10, None, 1, None, id="correct_external"),
        pytest.param(_cpp_files.main2, 5, None, 0.6, None, id="warnings_external"),
        pytest.param(_cpp_files.main3, 1, None, 0, None, id="error_external"),
        pytest.param(_cpp_files.main2, 10, 0.3, 0.4, None, id="warnings_external_change_penalty"),
        pytest.param(_cpp_files.main2, 30, 0.6, 0, None, id="warnings_external_penalty_cap"),
        pytest.param(_cpp_files.tested1, 50, None, 1, None, id="correct_internal"),
        pytest.param(_cpp_files.tested1, 15, None, 1, None, id="correct_internal_tester_warning"),
        pytest.param(_cpp_files.tested2, 5, None, 0.6, None, id="warning_internal"),
        pytest.param(_cpp_files.tested3, 10, None, 0, None, id="error_internal"),
        pytest.param(_cpp_files.tested2, 100, 0.35, 0.3, None, id="warning_internal_change_penalty"),
        pytest.param(_cpp_files.tested2, 10, 0.8, 0, None, id="warning_internal_penalty_cap"),
        pytest.param(_cpp_files.tested1, 12, None, 1, "o", id="correct_o"),
        pytest.param(_cpp_files.tested1, 15, None, 1, "o", id="correct_o_tester_warning"),
        pytest.param(_cpp_files.tested2, 5, None, 0.6, "o", id="warning_o"),
        pytest.param(_cpp_files.tested3, 20, None, 0, "o", id="error_o"),
        pytest.param(_cpp_files.tested2, 10, 0.1, 0.8, "o", id="warning_o_change_penalty"),
        pytest.param(_cpp_files.tested2, 10, 0.9, 0, "o", id="warning_o_penalty_cap"),
    ]
)
def test_compilation(tested_file, max_score, warning_penalty, expected_mark, check_type):
    config = dict_to_namedtuple({
        "tests":
            [
                {
                    **{"type": "compile", "max_score": max_score},
                    **({"type": check_type} if check_type is not None else {}),
                    **({"warning_penalty": warning_penalty} if warning_penalty is not None else {})
                }
            ]})
    tmp_files = TmpFiles(tested_file, config)
    runner = CppRunner(tmp_files)

    runner.test_compile(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks
    assert len(feedbacks) == 1
    feedback = feedbacks[0]

    assert feedback.name == "compilation"
    assert feedback.max_score == max_score
    assert feedback.score == expected_mark * max_score

    tmp_files.teardown()

@pytest.mark.parametrize(
    "tested_file, tester_file, max_score, expected_marks",
    [
        pytest.param(_cpp_files.tested4, _cpp_files.tester4, 10, [
            {"name": "test1", "max_score": 0.25, "score": 1},
            {"name": "test2", "max_score": 0.25, "score": 1},
            {"name": "test3", "max_score": 0.25, "score": 1},
            {"name": "test4", "max_score": 0.25, "score": 1},
            ], id="success"),
        pytest.param(_cpp_files.tested4, _cpp_files.tester4, 20, [
            {"name": "test1", "max_score": 0.25, "score": 1},
            {"name": "test2", "max_score": 0.25, "score": 1},
            {"name": "test3", "max_score": 0.25, "score": 1},
            {"name": "test4", "max_score": 0.25, "score": 1},
            ], id="success_o"),
        pytest.param(_cpp_files.tested4, _cpp_files.tester5, 15, [
            {"name": "test1", "max_score": 0.25, "score": 0.5},
            {"name": "test2", "max_score": 0.25, "score": 0.8},
            {"name": "test3", "max_score": 0.25, "score": 0.3},
            {"name": "test4", "max_score": 0.25, "score": 0.1}
        ], id="mistakes"),
        pytest.param(_cpp_files.tested4, _cpp_files.tester5, 30, [
            {"name": "test1", "max_score": 0.25, "score": 0.5},
            {"name": "test2", "max_score": 0.25, "score": 0.8},
            {"name": "test3", "max_score": 0.25, "score": 0.3},
            {"name": "test4", "max_score": 0.25, "score": 0.1}
        ], id="mistakes_o"),
    ]
)
def test_functionality_success(tested_file, tester_file, max_score, expected_marks):
    config = dict_to_namedtuple({
        "tests": [
            {
                "type": "functionality",
                "max_score": max_score,
                "tester_file": tester_file
            }
        ]})
    tmp_files = TmpFiles(tested_file, config)
    runner = CppRunner(tmp_files)
    runner.check_validity = False

    runner.test_functionality(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks
    assert len(feedbacks) == len(expected_marks)
    for expected, real in zip(expected_marks, feedbacks):
        assert real.name == expected["name"]
        assert round(real.max_score, 2) == round(expected["max_score"] * max_score, 2)
        assert round(real.score, 2) == round(expected["score"] * expected["max_score"] * max_score, 2)


    tmp_files.teardown()

@pytest.mark.parametrize(
    "tested_file, tester_file, max_score, expected_mark",
    [
        pytest.param(_cpp_files.tested1, _cpp_files.tester1, 1, 0, id="fail"),
        pytest.param(_cpp_files.tested1, _cpp_files.tester1, 1, 0, id="fail_o"),
    ]
)
def test_functionality_fail(tested_file, tester_file, max_score, expected_mark):    #TODO: Remove check type
    config = dict_to_namedtuple({
        "tests":[
            {
                "type": "functionality",
                "max_score": max_score,
                "tester_file": tester_file
            }
        ]})
    tmp_files = TmpFiles(tested_file, config)
    runner = CppRunner(tmp_files)

    runner.test_functionality(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks
    assert len(feedbacks) == 1
    feedback = feedbacks[0]

    assert feedback.name == "functionality"
    assert feedback.max_score == max_score
    assert feedback.score == 0

    tmp_files.teardown()


@pytest.mark.parametrize(
    "tested_file, tester_file, max_score, expected_marks",
    [
        pytest.param(_cpp_files.tested5, _cpp_files.tester1_py, 10, [
            {"name": "Q1", "max_score": 0.4, "score": 1},
            {"name": "Q2", "max_score": 0.6, "score": 1},
        ], id="success"),
        pytest.param(_cpp_files.tested5, _cpp_files.tester2_py, 10, [
            {"name": "Q1", "max_score": 0.4, "score": 0.5},
            {"name": "Q2", "max_score": 0.4, "score": 0.2},
            {"name": "Q3", "max_score": 0.2, "score": 0.8},
        ], id="mistakes"),
    ]
)
def test_functionality_executable_success(tested_file, tester_file, max_score, expected_marks):
    config = dict_to_namedtuple({
        "tests":[
            {
                "type": "functionality_executable",
                "max_score": max_score,
                "tester_file": tester_file
            }
        ]})
    tmp_files = TmpFiles(tested_file, config)
    runner = CppRunner(tmp_files)
    runner.check_validity = False

    runner.test_executable_functionality(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks
    assert len(feedbacks) == len(expected_marks)
    for expected, real in zip(expected_marks, feedbacks):
        assert real.name == expected["name"]
        assert round(real.max_score, 2) == round(expected["max_score"] * max_score, 2)
        assert round(real.score, 2) == round(expected["score"] * expected["max_score"] * max_score, 2)

    tmp_files.teardown()

@pytest.mark.parametrize(
    "tested_file, tester_file, max_score, expected_mark",
    [
        pytest.param(_cpp_files.tested5, _cpp_files.empty_tester_py, 1, 0, id="fail"),
    ]
)
def test_functionality_executable_fail(tested_file, tester_file, max_score, expected_mark):
    config = dict_to_namedtuple({
        "tests":[
            {
                "type": "functionality_executable",
                "max_score": max_score,
                "tester_file": tester_file
            }
        ]})
    tmp_files = TmpFiles(tested_file, config)
    runner = CppRunner(tmp_files)

    runner.test_executable_functionality(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks
    assert len(feedbacks) == 1
    feedback = feedbacks[0]

    assert feedback.name == "functionality"
    assert feedback.max_score == max_score
    assert feedback.score == 0

    tmp_files.teardown()

@pytest.mark.parametrize(
    "tested_file, tester_file, max_score, func_check_type",
    [
        pytest.param(_cpp_files.tested3, _cpp_files.tester3, 80, "functionality", id="internal"),
        pytest.param(_cpp_files.tested3, _cpp_files.empty_tester_py,  20, "functionality_executable", id="executable"),
    ]
)
def test_run_test_compilation_failed(tested_file, tester_file, max_score, func_check_type):
    config = dict_to_namedtuple({
        "tests":[
            {
                "type": func_check_type,
                "max_score": max_score,
                "tester_file": tester_file
            }
        ]})
    tmp_files = TmpFiles(tested_file, config)
    runner = CppRunner(tmp_files)

    feedbacks = runner.run_test(config.tests)

    assert len(feedbacks) == 1   

    placeholder_feedback = feedbacks[0]

    assert placeholder_feedback.name == "functionality"
    assert placeholder_feedback.max_score == max_score
    assert placeholder_feedback.score == 0

    tmp_files.teardown()

@pytest.mark.parametrize(
    "tested_file, max_score, expected_mark, error_penalty",
    [
        pytest.param(_cpp_files.main1, 1, 1, None, id="correct"),
        pytest.param(_cpp_files.main3, 0.1, 0.8, None, id="missing_bracket"),
        pytest.param(_cpp_files.main4, 0.5, 0.6, None, id="uninititialised_variable"),
        pytest.param(_cpp_files.main4, 0.6, 0.8, 0.1, id="changed_penalty"),
        pytest.param(_cpp_files.main4, 0.2, 0, 0.6, id="penalty_cap")
    ]
)
def test_static_code_analysis(tested_file, max_score, expected_mark, error_penalty):
    config = dict_to_namedtuple({
        "tests":[
            {
                **{"type":"static", "max_score": max_score},
                **({"error_penalty": error_penalty} if error_penalty is not None else {})
            }
        ]})
    tmp_files = TmpFiles(tested_file, config)
    runner = CppRunner(tmp_files)

    runner.static_code_analysis(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks
    assert len(feedbacks) == 1
    feedback = feedbacks[0]

    assert feedback.name == "static analysis"
    assert feedback.max_score == max_score
    assert round(feedback.score, 2) == round(expected_mark * max_score, 2)

    tmp_files.teardown()

@pytest.mark.parametrize(
    "tested_file, max_score, expected_mark",
    [
        pytest.param(_cpp_files.main1, 10, 1, id="correct"),
        pytest.param(_cpp_files.main4, 80, 0.83333, id="indent_mistake"),
        pytest.param(_cpp_files.main2, 60, 0.4, id="wrong_indent")
    ]
)
def test_code_style(tested_file, max_score, expected_mark):
    config = dict_to_namedtuple({
        "tests":[
            {
                "type": "style",
                "max_score": max_score
            }
        ]})
    tmp_files = TmpFiles(tested_file, config)
    runner = CppRunner(tmp_files)

    runner.test_code_style(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks
    assert len(feedbacks) == 1
    feedback = feedbacks[0]

    assert feedback.name == "style"
    assert feedback.max_score == max_score
    assert round(feedback.score, 2) == round(expected_mark * max_score, 2)

    tmp_files.teardown()