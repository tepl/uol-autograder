from tests import TEST_DIR
import pytest
from collections import namedtuple
from pathlib import Path

IntegrationCategories = namedtuple("IntegrationCategories", "cpp cpp_o py cpp_executable cpp_timeout py_exploit cpp_memory cpp_17std py_env")
IntegrationFiles = namedtuple("IntegrationFiles", "tested_file config_file expected_file")
_integration = Path(TEST_DIR, "fixtures/integration")

_integration_files = IntegrationCategories(
    IntegrationFiles(
        Path(_integration, "cpp/tested.cpp"),
        Path(_integration, "cpp/cpp_config.json"),
        Path(_integration, "cpp/expected_result.json")
    ),
    IntegrationFiles(
        Path(_integration, "cpp_o/tested.cpp"),
        Path(_integration, "cpp_o/cpp_config.json"),
        Path(_integration, "cpp_o/expected_result.json")
    ),
    IntegrationFiles(
        Path(_integration, "py/tested.py"),
        Path(_integration, "py/py_config.json"),
        Path(_integration, "py/expected_result.json")
    ),
    IntegrationFiles(
        Path(_integration, "cpp_executable/Math.cpp"),
        Path(_integration, "cpp_executable/config.json"),
        Path(_integration, "cpp_executable/expected_result.json")
    ),
    IntegrationFiles(
        Path(_integration, "cpp_timedout/Primes.cpp"),
        Path(_integration, "cpp_timedout/config.json"),
        Path(_integration, "cpp_timedout/expected_result.json")
    ),
    IntegrationFiles(
        Path(_integration, "py_exploit/tested.py"),
        Path(_integration, "py_exploit/py_config.json"),
        Path(_integration, "py_exploit/expected_result.json")
    ),
    IntegrationFiles(
        Path(_integration, "cpp_memory/Stack.cpp"),
        Path(_integration, "cpp_memory/config.json"),
        Path(_integration, "cpp_memory/expected_result.json")
    ),
    IntegrationFiles(
        Path(_integration, "cpp_17std/tested.cpp"),
        Path(_integration, "cpp_17std/cpp_config.json"),
        Path(_integration, "cpp_17std/expected_result.json")
    ),
    IntegrationFiles(
        Path(_integration, "py_env/tested.py"),
        Path(_integration, "py_env/py_config.json"),
        Path(_integration, "py_env/expected_result.json")
    ),
)

Py = namedtuple("Py", "syntax functionality static")
PySyntax = namedtuple("PySyntax", "correct incorrect")
PyFunctionality = namedtuple("PyFunctionality", "tester tested_0 tested_1 tested_2")
PyStatic = namedtuple("PyStatic", "correct warnings errors")
py = Path(TEST_DIR, "fixtures/py")

_py_files = Py(
    PySyntax(
        Path(py, "correct.py"),
        Path(py, "incorrect.py")
    ),
    PyFunctionality(
        Path(py, "tester.py"),
        Path(py, "tested_0.py"),
        Path(py, "tested_1.py"),
        Path(py, "tested_2.py")
    ),
    PyStatic(
        Path(py, "static_correct.py"),
        Path(py, "static_warnings.py"),
        Path(py, "static_errors.py")
    )
)

General = namedtuple("General", "py cpp empty empty_replacement other_replacement module")
CommentTiers = namedtuple("CommentTiers", "good ok none")
general = Path(TEST_DIR, "fixtures/general")
general_replacement = Path(TEST_DIR, "fixtures/general_replacement")

_general_files = General(
    CommentTiers(
        Path(general, "well_commented.py"),
        Path(general, "ok_commented.py"),
        Path(general, "no_comments.py")
    ),
    CommentTiers(
        Path(general, "well_commented.cpp"),
        Path(general, "ok_commented.cpp"),
        Path(general, "no_comments.cpp")
    ),
    Path(general, "empty.py"),
    Path(general_replacement, "empty.py"),
    Path(general_replacement, "other.py"),
    Path(general, "module.py")
)

SetupFiles = namedtuple("SetupFiles", "test_config tmp_files")
TmpFiles = namedtuple("TmpFiles", "tester_file tested_file additional_file_0 additional_file_1")
fixtures = Path(TEST_DIR, "fixtures/setup")

_setup_files = SetupFiles(
    Path(fixtures, "test_config.json"),
    TmpFiles(
        Path(fixtures, "tester_file.txt"),
        Path(fixtures, "tested_file.txt"),
        Path(fixtures, "additional_file_0.txt"),
        Path(fixtures, "additional_file_1.txt")
    )
)

Util = namedtuple("Util", "exit echo loop")
util = Path(TEST_DIR, "fixtures/util")

_util_files = Util(
    Path(util, "exit.py"),
    Path(util, "echo.py"),
    Path(util, "loop.py")
)

compile_files = [
    "main1.cpp",
    "main2.cpp",
    "main3.cpp",
    "main4.cpp",
    "empty_tester.py",
    "tester1.cpp",
    "tester1_warning.cpp",
    "tester2.cpp",
    "tester3.cpp",
    "tester4.cpp",
    "tester5.cpp",
    "tested1.h",
    "tested2.h",
    "tested3.h",
    "tested4.h",
    "tested1.cpp",
    "tested2.cpp",
    "tested3.cpp",
    "tested4.cpp",
    "tested5.cpp",
    "tester1.py",
    "tester2.py",
]
Cpp = namedtuple("Cpp", ' '.join([f.split(".")[0] if f.split(".")[-1] == "cpp" else f.replace(".", "_") for f in compile_files]))
cpp = Path(TEST_DIR, "fixtures/cpp")

_cpp_files = Cpp(
    *[Path(cpp, f) for f in compile_files]
)

@pytest.fixture
def integration_files():
    return _integration_files

@pytest.fixture
def setup_files():
    return _setup_files

@pytest.fixture
def util_files():
    return _util_files
    
@pytest.fixture
def general_files():
    return _general_files

@pytest.fixture
def py_files():
    return _py_files

@pytest.fixture
def cpp_files():
    return _cpp_files