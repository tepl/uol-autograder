import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

if int(sys.argv[1]) == 0:
    print(sys.argv[2])
else:
    eprint(sys.argv[2])