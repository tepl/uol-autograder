import sys, os
import json
import importlib.util
import argparse
import time
import random

parser = argparse.ArgumentParser(description="Test an exercise")
parser.add_argument('tested_file', type=str, help="file to be tested")
parser.add_argument('output_file', type=str, help="file to publish the result")
parser.add_argument('py_eval_module', type=str, help="python module with util functions")
parser.add_argument('secret_key', type=str)

def load_module(tested_file):
    spec = importlib.util.spec_from_file_location("tested_module", tested_file)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module

def test_file():
    args = parser.parse_args()

    assert os.path.isfile(args.tested_file), os.path.isfile(args.py_eval_module)

    # Import the tested file as a module
    tested_module = load_module(args.tested_file)

    with open(args.output_file, "w") as f:
        json.dump(tested_module.feedback, f)

if __name__ == "__main__":
    test_file()
