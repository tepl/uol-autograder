# Imports
import math

# Calculate the factorial of a number
# Args:
#   n - the number to calculate the factorial for (int)
# Returns:
#   the factorial of n
def factorial(n):
    if n < 1:
        raise ArithmeticError("Values less than one don't have a factorial")

    return 1 if int(n) == 1 else factorial(int(n) - 1) * int(n)
    

# Main function
def main():
    # Calculate and print the factorial of 5
    print(factorial(5))

# Entry point
if __name__ == "__main__":
    main()