import math

# Calculate the factorial of a number
def factorial(n):
    # Test for valid input values
    if n < 1:
        raise ArithmeticError("Values less than one don't have a factorial")
    # Calculate factorial
    return 1 if int(n) == 1 else factorial(int(n) - 1) * int(n)
    

def main():
    # Calculate and print the factorial of 5
    print(factorial(5))

if __name__ == "__main__":
    main()