import math

def factorial(n):
    if n < 1:
        raise ArithmeticError("Values less than one don't have a factorial")

    return 1 if int(n) == 1 else factorial(int(n) - 1) * int(n)
    

def main():
    print(factorial(5))

if __name__ == "__main__":
    main()