// General includes
#include <math.h>
// My includes
#include "test.h"

// Calculate the the nth fibonacci number
// args:
//  n - the index of the fibonacci number to calculate (int)
// output:
//  the nth fibonacci number (int)
int fibonacci(int n){
  // Check for less than 1 cases
  if(n <= 1){ return 0; }
  // Check for the special case of 2
  else if(n == 2) { return 1; }
  // Calculate fibonacci number recursively
  else { return fibonacci(n - 1) + fibonacci(n - 2); }
}