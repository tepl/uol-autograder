// Imports
#include <math.h>
#include "test.h"

// Calculate the the nth fibonacci number
int fibonacci(int n){
  // Check for special cases
  if(n <= 1){
    return 0;
  }
  else if(n == 2) {
    return 1;
  }
  else {
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
}