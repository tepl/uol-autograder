import argparse
import json


parser = argparse.ArgumentParser(description="Test an exercise")
parser.add_argument('tested_executable', type=str, help="file to be tested")
parser.add_argument('output_file', type=str, help="file to publish the result")
parser.add_argument('py_eval_module', type=str, help="python module with util functions")
parser.add_argument('secret_key', type=str)


def test_file():
    args = parser.parse_args()

    feedback = [
        {
            "question": "Q1",
            "mark": round(0.5, 2),
            "weight": 0.4,
            "feedback": ""
        },
        {
            "question": "Q2",
            "mark": round(0.2, 2),
            "weight": 0.4,
            "feedback": ""
        },
        {
            "question": "Q3",
            "mark": round(0.8, 2),
            "weight": 0.2,
            "feedback": ""
        }
    ]

    with open(args.output_file, "w") as f:
        json.dump(feedback, f)

if __name__ == "__main__":
    test_file()