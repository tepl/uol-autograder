#include "tested4.h"
#include "json.hpp"
#include "cpp_eval_util.h"

using namespace std;
using namespace nlohmann;


json test(string name, float x, string feedback){
  float score = run_test(x);

  char s_score[5];
  std::snprintf(s_score, 5, "%.2f", score);

  json result = {
    {"question", name},
    {"mark", s_score},
    {"weight", 0.25f},
    {"feedback", feedback}
  };
  
  return result;
}

int main(int argc, char **argv) {
  if(argc < 2){
    return 1;
  }

  ofstream output(argv[1]);
  vector<json> combined_results = {};

  combined_results.push_back(test("test1", 0.5f, "feedback"));
  combined_results.push_back(test("test2", 0.8f, "feedback"));
  combined_results.push_back(test("test3", 0.3f, "feedback"));
  combined_results.push_back(test("test4", 0.1f, "feedback"));

  json json_result = {
    combined_results
  };
  output << json_result[0].dump(4);
  return 0;
}