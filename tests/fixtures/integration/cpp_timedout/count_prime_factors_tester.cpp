#include "Primes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 8

class CountPrimeFactorsEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
        {315, 4},
        {1485, 5},
        {10, 2},
        {0, 0},
        {-10, 0},
        {999, 4},
        {1248, 7},
        {44100, 8}
    };

    CountPrimeFactorsEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "count_prime_factors(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return count_prime_factors(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  CountPrimeFactorsEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
