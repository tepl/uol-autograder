#include "Primes.h"
// import any required libraries here
#include <math.h>
#include <cstring>

using namespace std;

bool is_prime(int n) {
  // https://www.geeksforgeeks.org/c-program-to-check-prime-number/
  // Corner case
  if (n <= 1) return false;

  while(n == 17) {}

  // Check from 2 to n-1
  for (int i = 2; i < n; i++)
    if (n % i == 0) return false;

  return true;
}

int nth_prime(int n) {
  if (n < 1 || n > 1000) {
    return 0;
  }
  // https://www.geeksforgeeks.org/program-to-find-the-nth-prime-number/
  const int max_size = 10000;
  bool IsPrime[max_size];
  memset(IsPrime, true, sizeof(IsPrime));

  for (int p = 2; p * p < max_size; p++) {
    // If IsPrime[p] is not changed, then it is a prime
    if (IsPrime[p] == true) {
      // Update all multiples of p greater than or
      // equal to the square of it
      // numbers which are multiple of p and are
      // less than p^2 are already been marked.
      for (int i = p * p; i < max_size; i += p) IsPrime[i] = false;
    }
  }

  int counter = 0;

  // Store all prime numbers
  for (int p = 2; p < max_size; p++) {
    if (IsPrime[p]) {
      counter++;
      if (counter == n) {
        return p;
      }
    }
  }
  return 0;
}

bool is_sum_of_two_primes(int n) {
  if (n < 1) {
    return false;
  }
  for (int i = 0; i < n; i++) {
    if (is_prime(i) && is_prime(n - i)) {
      return true;
    }
  }
  return false;
}

int count_prime_factors(int n) {
  if (n < 1) {
    return false;
  }
  // https://www.geeksforgeeks.org/print-all-prime-factors-of-a-given-number/
  // Print the number of 2s that divide n
  int count = 0;
  while (n % 2 == 0) {
    count++;
    n = n / 2;
  }

  // n must be odd at this point. So we can skip
  // one element (Note i = i +2)
  for (int i = 3; i <= sqrt(n); i = i + 2) {
    // While i divides n, print i and divide n
    while (n % i == 0) {
      count++;
      n = n / i;
    }
  }

  // This condition is to handle the case when n
  // is a prime number greater than 2
  if (n > 2) count++;

  return count;
}

int gcd(int a, int b) {
  int d = 0;
  while (a % 2 == 0 && b % 2 == 0) {
    a /= 2;
    b /= 2;
    d += 1;
  }
  while (a != b) {
    if (a % 2 == 0)
      a /= 2;
    else if (b % 2 == 0)
      b /= 2;
    else if (a > b)
      a = (a - b) / 2;
    else
      b = (b - a) / 2;
  }
  return a * pow(2, d);
}

bool are_coprimes(int n, int m) {
  if (n < 1 || m < 1) {
    return false;
  }
  return gcd(n, m) == 1;
}

int number_of_coprimes(int p, int q) {
  if (p < 1 || !is_prime(p) || q < 1 || !is_prime(q)) {
    return 0;
  }
  return (p - 1) * (q - 1);
}