#include "Primes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 8

class IsSumOfTwoPrimesEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][2] = {
        {20, 1},
        {11, 0},
        {5, 1},
        {0, 0},
        {-10, 0},
        {999, 1},
        {997, 0},
        {31, 1}
    };

    IsSumOfTwoPrimesEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "is_sum_of_two_primes(" + to_string(test_cases[i][0]) + ")";
    }

    int GetResult(int i){
      return is_sum_of_two_primes(test_cases[i][0]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][1] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][1]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  IsSumOfTwoPrimesEvaluator evaluator(argc, argv);
  return evaluator.Run();
}
