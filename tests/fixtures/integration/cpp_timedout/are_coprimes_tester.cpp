#include "Primes.h"
#include "cpp_eval_util.h"
#include <string>
#include <iostream>
#include <exception>

using namespace std;

#define TEST_CASE_COUNT 12

class AreCoprimesEvaluator : public Evaluator<int>{
  public:
    int test_cases[TEST_CASE_COUNT][3] = {
        {5, 6, 1},
        {8, 16, 0},
        {1, 0, 0},
        {-10, 5, 0},
        {3, 0, 0},
        {51, 45, 0},
        {51, 46, 1},
        {100, 101, 1},
        {101, 101, 0},
        {101, 100, 1},
        {3, 33, 0},
        {71, 7, 1}
    };

    AreCoprimesEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      if(i >= TEST_CASE_COUNT) { return ""; }
      return "are_coprimes(" + to_string(test_cases[i][0]) + ", " + to_string(test_cases[i][1]) + ")";
    }

    int GetResult(int i){
      return are_coprimes(test_cases[i][0], test_cases[i][1]);
    }

    float GetScore(int i, int result){
      return result == test_cases[i][2] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, int result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(test_cases[i][2]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  AreCoprimesEvaluator evaluator(argc, argv);
  return evaluator.Run();
}

