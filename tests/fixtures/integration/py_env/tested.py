import numpy as np

def sumdiff(x, y):
	if x == 1:
		x = x / 0
	return x+y, x-y

def fibonacci(n): 
    if n <= 1:
        return 0

    sqrtFive = np.sqrt(5)
    alpha = (1 + sqrtFive) / 2
    beta = (1 - sqrtFive) / 2

    return int(np.rint(((alpha ** (n - 1)) - (beta ** (n - 1))) / sqrtFive))
		