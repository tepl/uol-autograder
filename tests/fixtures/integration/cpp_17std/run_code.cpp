#include "test.h"
#include "json.hpp"
#include "cpp_eval_util.h"
#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <exception>
#include <tuple>
#include <math.h>

using namespace std;
using namespace nlohmann;

int test1_cases[][2] = {
  {0, 0},
  {1, 1},
  {10, 55},
  {100, 5050},
  {1000, 500500}
};
int test1_case_count = 5;

json test1(){
  vector<string> results = {};
  vector<string> test_values = {};

  for(int i = 0; i < test1_case_count; i++)
  {
    int result;
    try
    {
      result = sum(test1_cases[i][0]);
      results.push_back(result == test1_cases[i][1] ? "pass" : "fail");
    }
    catch (exception& e)
    {
      results.push_back(e.what());
    }
    test_values.push_back(to_string(test1_cases[i][0]));
  }

  string feedback;
  float score;
  char s_score[5];

  tie(feedback, score) = process_result(results, test_values);

  auto written = std::snprintf(s_score, 5, "%.2f", score);

  json result = {
    {"question", "sum"},
    {"mark", s_score},
    {"weight", 0.4},
    {"feedback", feedback}
  };
  
  return result;
}

int test2_cases[][2] = {
	{-5, 0}, {-4, 0}, {-3, 0}, {-2, 0}, {-1, 0},
	{0, 0}, {1, 0}, {2, 1}, {3, 1}, {4, 2},
	{5, 3}, {6, 5}, {7, 8}, {8, 13}, {9, 21},
	{10, 34}, {11, 55}, {12, 89}, {13, 144}, {14, 233},
	{15, 377}, {16, 610}, {17, 987}, {18, 1597}, {19, 2584}
};
int test2_case_count = 25;

json test2(){
  vector<string> results = {};
  vector<string> test_values = {};

  for(int i = 0; i < test2_case_count; i++)
  {
    int result;
    try
    {
      result = fibonacci(test2_cases[i][0]);
      results.push_back(result == test2_cases[i][1] ? "pass" : "fail");
    }
    catch (exception& e)
    {
      results.push_back(e.what());
    }
    test_values.push_back(to_string(test2_cases[i][0]));
  }

  string feedback;
  float score;
  char s_score[5];

  tie(feedback, score) = process_result(results, test_values);

  std::snprintf(s_score, 5, "%.2f", score);

  json result = {
    {"question", "fibonacci"},
    {"mark", s_score},
    {"weight", 0.6},
    {"feedback", feedback}
  };
  
  return result;
}

int main(int argc, char **argv) {
  if(argc < 2){
    return 1;
  }

  ofstream output(argv[1]);
  vector<json> combined_results = {};

  combined_results.push_back(test1());
  combined_results.push_back(test2());

  json json_result = {
    combined_results
  };
  output << json_result[0].dump(4);
  return 0;
}