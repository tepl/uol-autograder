#include "cpp_eval_util.h"
#include <string>
#include "Stack.h"

using namespace std;

#define TEST_CASE_COUNT 8

long case1(){
    Stack s;
    s.Push(10);
    s.Push(20);
    s.Push(100);
    return s.Pop();
}
long case2(){
    Stack s;
    s.Push(14);
    return s.Pop();
}
long case3(){
    Stack s;
    s.Push(10);
    s.Pop();
    return s.Pop();
}
long case4(){
    Stack s;
    s.Push(8641);
    s.Push(31415);
    s.Pop();
    s.Push(16);
    s.Push(1);
    s.Pop();
    s.Push(36156);
    s.Push(16815);
    s.Pop();
    s.Pop();
    s.Pop();
    s.Push(-85416);
    s.Push(9841);
    s.Push(314);
    s.Push(5865);
    s.Pop();
    s.Push(52);
    s.Push(24);
    s.Pop();
    s.Pop();
    return s.Pop();
}
long case5(){
    Stack s;
    s.Push(8641);
    s.Push(31415);
    s.Pop();
    s.Push(16);
    s.Push(1);
    s.Pop();
    s.Push(36156);
    s.Push(16815);
    s.Pop();
    s.Pop();
    s.Pop();
    s.Pop();
    s.Pop();
    s.Pop();
    return s.Pop();
}
long case6(){
    Stack s;
    for(int i = 0; i < 1e4; i++){
        s.Pop();
    }
    return s.Pop();
}
long case7(){
    Stack s;
    for(int i = 0; i < 1e6; i++){
        s.Push(i);
    }
    s.Push(1000000);
    return s.Pop();
}
long case8(){
    Stack s;
    for(int i = 0; i < 90000000; i++){   // This should exhaust the allocated 256MB limit
        s.Push(10);
        s.Pop();
    }
    s.Push(10);
    return s.Pop();
}

class StackEvaluator : public Evaluator<long>{
  public:
    string test_names[TEST_CASE_COUNT] = {
        "Stack: Push 3 Pop 1",
        "Stack: Push 1 Pop 1",
        "Stack: Push 1 Pop 2",
        "Stack: Push Pop mix 1",
        "Stack: Push Pop mix 2",
        "Stack: Pop many",
        "Stack: Push many Pop 1",
        "Stack: Push Pop many",
    };

    long expected[TEST_CASE_COUNT] = {
        100, 14, -1, 314, -1, -1, 1000000, 10
    };

    long (*functptr[TEST_CASE_COUNT])() = { case1, case2, case3, case4, case5, case6, case7, case8 } ;

    StackEvaluator(int argc, char** argv):Evaluator(argc, argv){}

    string GetName(int i){
      return i < TEST_CASE_COUNT ? test_names[i] : "";
    }

    long GetResult(int i){
      return (*functptr[i])();
    }

    float GetScore(int i, long result){
      return result == expected[i] ? 1.0f : 0.0f;
    }

    string GetFeedback(int i, long result, float score){
      return "Returned " + to_string(result) + ", expecting " + to_string(expected[i]) + (score >= 1 ? " : PASS!" : " : FAIL!");
    }
};

int main(int argc, char **argv) {
  StackEvaluator evaluator(argc, argv);
  return evaluator.Run();
}