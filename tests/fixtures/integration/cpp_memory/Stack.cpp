#include "Stack.h"

Stack::Stack() : _head(nullptr){}

void Stack::Push(long v){
    LinkedItem *new_head = new LinkedItem;
    new_head->value = v;
    new_head->next = _head;
    _head = new_head;
}

long Stack::Pop(){
    if(_head == nullptr){
        return -1;
    }
    LinkedItem *temp = _head;
    long val = _head->value;
    _head = _head->next;
    // delete temp;
    return val;
}