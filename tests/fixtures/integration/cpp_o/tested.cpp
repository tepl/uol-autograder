#include "test.h"

int sum(int n) {

  int sum = 0;

  for (int i=0; i<=n; i++) {
    sum+=i;
  }
  
  return sum;

}

int fibonacci(int n){
  if(n <= 1){
    return 0;
  }
  else if(n == 2) {
    return 1;
  }
  else {
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
}