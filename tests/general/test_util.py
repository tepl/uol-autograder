import os
from pathlib import Path
from feedback.general.util import get_current_dir, Lookup, create_feedback_json
from feedback.general.execution import execute
from feedback.general.constants import PY_RUNNER
from tests import TEST_DIR
from tests.fixtures import util_files

def test_get_current_dir():
    current_dir = Path(get_current_dir())
    print(current_dir)
    assert current_dir.is_dir
    assert current_dir.stem == "general"
    assert current_dir.parent.stem == "tests"

def test_lookup():
    class TestLookup(Lookup):
        _foo = "foo.txt"
    
    lookup = TestLookup(TEST_DIR)

    assert hasattr(lookup, "foo")
    assert Path(TEST_DIR, "foo.txt") == lookup.foo

def test_execute_retval(util_files, tmpdir):
    ext = util_files.exit.absolute().as_posix()
    exec_result = execute([PY_RUNNER, ext, "0"], tmpdir)
    assert exec_result.retval == 0

    exec_result = execute([PY_RUNNER, ext, "1"], tmpdir)
    assert exec_result.retval == 1

def strip_echo(echo):
    return echo.replace('"', "").replace("\r", "").replace("\n", "")

def test_executable_stdout(util_files, tmpdir):
    echo = util_files.echo.absolute().as_posix()
    text = "hello world"
    exec_result = execute([PY_RUNNER, echo, "0", text], tmpdir)
    assert strip_echo(exec_result.stdout) == text

def test_executable_stderr(util_files, tmpdir):
    echo = util_files.echo.absolute().as_posix()
    text = "hello world"
    exec_result = execute([PY_RUNNER, echo, "1", text], tmpdir)
    assert strip_echo(exec_result.stderr) == text

def test_executable_timeout(util_files, tmpdir):
    loop = util_files.loop.absolute().as_posix()
    exec_result = execute([PY_RUNNER, loop], tmpdir, timeout=1)
    assert exec_result.retval is None

