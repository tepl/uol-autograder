import pytest
from pathlib import Path
from tests.fixtures import _general_files
from feedback import load_config, TmpFiles
from feedback.general import Runner, GeneralLookup
from feedback.general.constants import *
from feedback.general.util import dict_to_namedtuple

@pytest.mark.parametrize(
    "tested_file,max_score,expected_mark",
    [
        pytest.param(_general_files.py.good, 10, 1, id="py-good"),
        pytest.param(_general_files.py.ok, 5, 0.808, id="py-ok"),
        pytest.param(_general_files.py.none, 20, 0, id="py-none"),
        pytest.param(_general_files.cpp.good, 10, 1, id="cpp-good"),
        pytest.param(_general_files.cpp.ok, 60, 0.1875, id="cpp-ok"),
        pytest.param(_general_files.cpp.none, 20, 0, id="cpp-none"),
        pytest.param(_general_files.empty, 15, 0, id="empty")
    ]
)
def test_comment_check(tested_file, max_score, expected_mark):
    config = dict_to_namedtuple({
        "tests":[
           {
               "type": "comments",
               "max_score": max_score
            }
        ]})

    tmp_files = TmpFiles(tested_file, config)

    runner = Runner(tmp_files)

    runner.test_comments(dict_to_namedtuple(config.tests[0]))

    feedbacks = runner.feedbacks

    assert len(feedbacks) == 1
    feedback = feedbacks[0]

    assert feedback.name == "comments"
    assert feedback.max_score == max_score
    assert round(feedback.score, 2) == round(max_score * expected_mark, 2)

    tmp_files.teardown()

def test_lookup():
    tmp_files = TmpFiles(_general_files.py.good, None)
    lookup = GeneralLookup(tmp_files.lookup_dir)

    assert lookup.comment_feedback.is_file()

def test_file_replacement_file_exists():
    config = dict_to_namedtuple({
        "tests":[
           {
               "type": "replace_files",
               "files": [
                   _general_files.empty_replacement.absolute().as_posix()
               ]
            }
        ]})

    tested_file = _general_files.empty

    tmp_files = TmpFiles(tested_file, config)

    target_path = Path(tmp_files.tmp_dir.name, _general_files.empty_replacement.name)

    with target_path.open() as f:
        original_content = f.read()
    
    with _general_files.empty_replacement.open() as f:
        target_content = f.read()

    assert original_content != target_content

    runner = Runner(tmp_files)

    runner.replace_files(dict_to_namedtuple(config.tests[0]))

    with target_path.open() as f:
        replaced_content = f.read()
    
    assert replaced_content == target_content

def test_file_replacement_file_missing():
    config = dict_to_namedtuple({
        "tests":[
           {
               "type": "replace_files",
               "files": [
                   _general_files.other_replacement.absolute().as_posix()
               ]
            }
        ]})

    tested_file = _general_files.empty

    tmp_files = TmpFiles(tested_file, config)

    target_path = Path(tmp_files.tmp_dir.name, _general_files.other_replacement.name)

    assert not target_path.is_file()
    
    with _general_files.other_replacement.open() as f:
        target_content = f.read()

    runner = Runner(tmp_files)

    runner.replace_files(dict_to_namedtuple(config.tests[0]))

    with target_path.open() as f:
        replaced_content = f.read()
    
    assert replaced_content == target_content


def _test_run_executable_base_test(*args, **kwargs):    
    tested_file = _general_files.empty
    tmp_files = TmpFiles(tested_file, {"tests":[]})

    runner = Runner(tmp_files)

    results = {}

    def mock_get_results(*args, **kwargs):
        results["args"] = args
        results["kwargs"] = kwargs
        return None


    runner._get_results = mock_get_results

    runner._test_run_executable(*args, **kwargs)

    mock_args = results.get("args", None)
    mock_kwargs = results.get("kwargs", None)

    assert len(mock_args) == 2
    assert len(mock_kwargs) == 4    

    return mock_args, mock_kwargs

def test_run_executable_defaults_applied_correctly():
    base_command = ["base", "command"]
    args, kwargs = _test_run_executable_base_test(base_command, {})
    assert args[1] == base_command
    assert kwargs.get('child_limit', None) == 0
    assert kwargs.get('executable_timeout', None) == EXECUTABLE_TIMEOUT
    assert kwargs.get('memory_limit', None) == MAX_VIRTUAL_MEMORY
    assert kwargs.get('allow_connections', None) == False

@pytest.mark.parametrize(
    "config, expected",
    [
        pytest.param({"timeout": 1}, 1, id="timeout: 1"),
        pytest.param({"timeout": -1}, -1, id="timeout: -1"),
        pytest.param({"timeout": 10}, 10, id="timeout: 10"),
    ]
)
def test_run_executable_timeout_passed_correctly(config, expected):
    config = dict_to_namedtuple(config)
    
    base_command = ["base", "command"]
    _, kwargs = _test_run_executable_base_test(base_command, config)
    assert kwargs.get('executable_timeout', None) == expected

@pytest.mark.parametrize(
    "config, expected",
    [
        pytest.param({"allow_connections": True}, True, id="Allow sockets"),
        pytest.param({"allow_connections": False}, False, id="Forbid sockets"),
    ]
)
def test_run_executable_allow_connections_passed_correctly(config, expected):
    config = dict_to_namedtuple(config)
    
    base_command = ["base", "command"]
    _, kwargs = _test_run_executable_base_test(base_command, config)
    assert kwargs.get('allow_connections', None) == expected

@pytest.mark.parametrize(
    "config, expected",
    [
        pytest.param({"memory_limit": 0}, 0, id="Memory limit: 0"),
        pytest.param({"memory_limit_mb": 128}, 134217728, id="Memory limit: 128mb"),
        pytest.param({"memory_limit": -1}, -1, id="Memory limit: -1"),
    ]
)
def test_run_executable_memory_limit_passed_correctly(config, expected):
    config = dict_to_namedtuple(config)
    
    base_command = ["base", "command"]
    _, kwargs = _test_run_executable_base_test(base_command, config)
    assert kwargs.get('memory_limit', None) == expected

@pytest.mark.parametrize(
    "child_limit, config, expected",
    [
        pytest.param(0, {}, 0, id="child_limit: default 0, no config"),
        pytest.param(10, {}, 10, id="child_limit: default 10, no config"),
        pytest.param(-1, {}, -1, id="child_limit: default disabled, no config"),
        pytest.param(0, {"child_process_limit": 1}, 1, id="child_limit: default 0, config 1"),
        pytest.param(1, {"child_process_limit": 1}, 2, id="child_limit: default 1, config 1"),
        pytest.param(-1, {"child_process_limit": 1}, -1, id="child_limit: default disabled, config 1"),
        pytest.param(1, {"child_process_limit": -1}, -1, id="child_limit: default 1, config disabled"),
    ]
)
def test_run_executable_child_limit_passed_correctly(child_limit, config, expected):
    config = dict_to_namedtuple(config)

    base_command = ["base", "command"]
    _, kwargs = _test_run_executable_base_test(base_command, config, child_limit=child_limit)
    assert kwargs.get('child_limit', None) == expected