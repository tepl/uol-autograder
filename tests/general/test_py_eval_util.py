import pytest
import inspect
from tests.fixtures import _general_files
from feedback.general import py_eval_util


def test_load_module():
    module = py_eval_util.load_module(_general_files.module)

    assert module

    assert inspect.ismodule(module)

    assert module.__name__ == "tested_module"


def test_get_module_functions():
    module = py_eval_util.load_module(_general_files.module)

    functions = py_eval_util.get_module_functions(module)

    assert functions

    assert len(functions) == 2

    assert "func1" in [func.__name__ for func in functions]
    assert "func2" in [func.__name__ for func in functions]


def test_get_module_classes():
    module = py_eval_util.load_module(_general_files.module)

    classes = py_eval_util.get_module_classes(module)

    assert classes

    assert len(classes) == 2

    assert "Class1" in [func.__name__ for func in classes]
    assert "Class2" in [func.__name__ for func in classes]


@pytest.mark.parametrize(
    "a,b,margin,result",
    [
        pytest.param(1, 2, 0.01, False),
        pytest.param(1, 1.00001, 0.01, True),
        pytest.param(1, 0.9999, 0.01, True),
        pytest.param(-1, -0.9999, 0.01, True),
        pytest.param(-1, 0.9999, 0.01, False),
        pytest.param(1, 2, 1, False),
        pytest.param(1, 2, 10, True),
    ]
)
def test_numbers_close(a, b, margin, result):
    assert py_eval_util.numbers_close(a, b, margin) == result


@pytest.mark.parametrize(
    "s1,s2,value",
    [
        ("hello", "world", 4),
        ("cat", "mat", 1),
        ("foo", "food", 1),
        ("abracadabra", "aaaaaaaa", 6),
        ("soup", "superb", 4),
    ]
)
def test_levenshtein_distance(s1, s2, value):
    assert py_eval_util.levenshtein_distance(s1, s2) == value

@pytest.mark.parametrize(
    "s1,s2,value",
    [
        ("hello", "world", 0.2),
        ("cat", "mat", 0.6666),
        ("foo", "food", 0.8571),
        ("abracadabra", "aaaaaaaa", 0.5263),
        ("soup", "superb", 0.6),
    ]
)
def test_levenshtein_ratio(s1, s2, value):
    assert py_eval_util.numbers_close(py_eval_util.levenshtein_ratio(s1, s2), value, 0.01)


def test_result_to_dictionary():
    question = "Question"
    mark = 0.5
    weight = 0.1
    feedback = "Some feedback"

    d = py_eval_util.result_to_dictionary(question, mark, weight, feedback)

    assert d["question"] == question
    assert d["mark"] == mark
    assert d["weight"] == weight
    assert d["feedback"] == feedback
