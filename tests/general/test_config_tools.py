import pytest
import feedback.general.config_tools as tools
from feedback.general.constants import MAX_VIRTUAL_MEMORY, EXECUTABLE_TIMEOUT
from feedback.general.util import dict_to_namedtuple

@pytest.mark.parametrize(
    "config, expected",
    [
        pytest.param({"memory_limit": 0}, 0, id="Memory limit: 0"),
        pytest.param({"memory_limit_mb": 128}, 128*1024*1024, id="Memory limit: 128mb"),
        pytest.param({"memory_limit": -1}, -1, id="Memory limit: -1"),
        pytest.param({"memory_limit_kb": 256}, 256*1024, id="Memory limit: 256kb"),
        pytest.param({"memory_limit_kb": 256, "memory_limit": -1}, -1, id="Memory limit: multiple, select disabled"),
        pytest.param({"memory_limit_mb": 1, "memory_limit_kb": 2048}, 1024*1024, id="Memory limit: multiple, select lowest"),
    ]
)
def test_get_memory_limit_handles_config(config, expected):
    config = dict_to_namedtuple(config)
    
    assert tools.get_memory_limit(config) == expected

def test_get_memory_limit_correct_default():
    assert tools.get_memory_limit(None) == MAX_VIRTUAL_MEMORY

@pytest.mark.parametrize(
    "default",
    [
        pytest.param(0, id="Default: 0"),
        pytest.param(10, id="Default: 10")
    ]
)
def test_get_memory_limit_handles_default(default):
    assert tools.get_memory_limit(None, default=default) == default


@pytest.mark.parametrize(
    "config, expected",
    [
        pytest.param({"timeout": 0}, 0, id="Timeout: 0"),
        pytest.param({"timeout": 10}, 10, id="Timeout: 10"),
        pytest.param({"timeout_sec": 100}, 100, id="Timeout_sec: 100"),
        pytest.param({"timeout_min": 3}, 3*60, id="Timeout_min: 3"),
        pytest.param({"timeout_ms": 1500}, 1.5, id="Timeout_ms: 1500"),
        pytest.param({"timeout": -10}, -1, id="Timeout_ms: disabled"),
        pytest.param({"timeout_ms": -1}, -1, id="Timeout_ms: disabled"),
        pytest.param({"timeout": -1, "timeout_sec": 1}, -1, id="Timeout: multiple, select disabled"),
        pytest.param({"timeout_min": 2, "timeout_sec": 60}, 60, id="Timeout: multiple, select smallest"),
    ]
)
def test_get_timeout_handles_config(config, expected):
    config = dict_to_namedtuple(config)
    
    assert tools.get_timeout(config) == expected

def test_get_timeout_correct_default():
    assert tools.get_timeout(None) == EXECUTABLE_TIMEOUT

@pytest.mark.parametrize(
    "default",
    [
        pytest.param(0, id="Default: 0"),
        pytest.param(10, id="Default: 10")
    ]
)
def test_get_timeout_handles_default(default):
    assert tools.get_timeout(None, default=default) == default