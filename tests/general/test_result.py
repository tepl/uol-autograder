from feedback.general.result import CheckResult
from feedback.general.util import dict_to_namedtuple

def test_output_passed():
    output = "__output__"
    result = CheckResult({}, "", 0, output)

    d = result.to_dict()

    assert result.output == output
    assert "output" in d
    assert d["output"] == output

def test_name_passed():
    name = "__output__"
    result = CheckResult({}, name, 0, "")

    d = result.to_dict()

    assert result.name == name
    assert "name" in d
    assert d["name"] == name

def test_tags_passed():
    tags = ["tag1", "tag2"]
    config = {
        "tags": tags
    }
    result = CheckResult(dict_to_namedtuple(config), "", 0, "")

    d = result.to_dict()

    assert result.tags == tags
    assert "tags" in d
    assert d["tags"] == tags

def test_score_calculation():
    max_score = 20
    score = 0.5
    config = {
        "max_score": max_score
    }
    result = CheckResult(dict_to_namedtuple(config), "", score, "")
    d = result.to_dict()

    assert result.max_score == max_score
    assert result.score == max_score * score
    assert "max_score" in d
    assert d["max_score"] == max_score
    assert "score" in d
    assert d["score"] == score * max_score

def test_number_passed():
    number = 3
    config = {
        "number": number
    }
    result = CheckResult(dict_to_namedtuple(config), "", 0, "")
    d = result.to_dict()

    assert result.number == str(number)
    assert "number" in d
    assert d["number"] == str(number)

def test_number_ommitted():
    result = CheckResult({}, "", 0, "")
    d = result.to_dict()

    assert result.number == None
    assert "number" not in d

def test_nested_score():
    max_score = 20
    sub_weight = 0.8
    score = 0.5
    config = {
        "max_score": max_score
    }
    result = CheckResult(dict_to_namedtuple(config), "", score, "", sub_weight=sub_weight)
    d = result.to_dict()

    assert result.max_score == max_score * sub_weight
    assert result.score == max_score * score * sub_weight
    assert "max_score" in d
    assert d["max_score"] == max_score * sub_weight
    assert "score" in d
    assert d["score"] == score * max_score * sub_weight

def test_nested_number():
    number = 3
    sub_number = 1
    config = {
        "number": number
    }
    result = CheckResult(dict_to_namedtuple(config), "", 0, "", sub_number)
    d = result.to_dict()

    assert result.number == f"{number}.{sub_number}"
    assert "number" in d
    assert d["number"] == f"{number}.{sub_number}"

def test_precision():
    precision = 1
    CheckResult.precision = precision
    max_score = 20.2384
    score = 0.3333
    config = {
        "max_score": max_score
    }
    result = CheckResult(dict_to_namedtuple(config), "", score, "")
    d = result.to_dict()

    assert result.max_score == round(max_score, precision)
    assert result.score == round(max_score * score, precision)
    assert d["max_score"] == round(max_score, precision)
    assert d["score"] == round(max_score * score, precision)


def test_visibility():
    visibility = "after_published"
    config = {
        "visibility": visibility
    }
    result = CheckResult(dict_to_namedtuple(config), "", 0, "")
    d = result.to_dict()

    assert result.visibility == visibility
    assert "visibility" in d
    assert d["visibility"] == visibility

def test_visibility_ommited():
    result = CheckResult({}, "", 0, "")
    d = result.to_dict()

    assert result.visibility == None
    assert "visibility" not in d
