from ..general.constants import PY_RUNNER, PY_VENV_EXECUTABLE_PATH, PY_VENV_PIP_PATH
from pathlib import Path
import subprocess


class EnvManager:
    def __init__(self, tmp_files, verbose=False):
        self._executables = {}
        self._envs_dir = Path(tmp_files.tmp_dir.name, ".envs")
        self._verbose = verbose

    def get_executable(self, requirements):
        if requirements is None:
            return PY_RUNNER

        key = None
        if isinstance(requirements, str):
            key = requirements
        elif isinstance(requirements, list):
            key = tuple(sorted(requirements))
        else:
            raise Exception("Invalid requirements")

        if key in self._executables:
            return self._executables[key]
        executable = self.create_executable(requirements)
        self._executables[key] = executable
        return executable

    def create_executable(self, requirements):
        env_name = f".env_{len(self._executables)}"
        env_dir = self._envs_dir.joinpath(env_name)
        env_dir.mkdir(parents=True)
        subprocess.check_call([PY_RUNNER, "-m", "venv", env_dir.absolute().as_posix()])

        pip_executable = env_dir.joinpath(PY_VENV_PIP_PATH)

        if isinstance(requirements, str):
            res = subprocess.run([pip_executable, "install", "-r", requirements], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        elif isinstance(requirements, list):
            res = subprocess.run([pip_executable, "install", *requirements], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        else:
            raise Exception("Invalid requirements")
        if self._verbose:
            print(f"Creating environment: {requirements}")
            print(res.stdout.decode())
            print(res.stderr.decode())

        return env_dir.joinpath(PY_VENV_EXECUTABLE_PATH).absolute().as_posix()




